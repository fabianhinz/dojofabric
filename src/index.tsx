import React from "react";
import ReactDOM from "react-dom";
import {
  Text,
  Stack,
  Fabric,
  Separator,
  TextField,
  getTheme,
  IStackStyles,
  ISeparatorStyles
} from "office-ui-fabric-react";
import "./index.css";

const fabricTheme = getTheme();

const rootStackStyles: IStackStyles = {
  root: {
    height: "100vh",
    width: "100vw",
    padding: fabricTheme.spacing.l2
  }
};

const separatorStyles: Partial<ISeparatorStyles> = {
  root: {
    boxShadow: fabricTheme.effects.elevation4,
    padding: fabricTheme.spacing.m,
    borderRadius: fabricTheme.effects.roundedCorner2,
    marginBottom: fabricTheme.spacing.l2
  }
};

const App = () => (
  <Stack styles={rootStackStyles}>
    <Separator styles={separatorStyles}>
      <Text variant="xxLarge">Welcome2UiFabric</Text>
    </Separator>

    <Stack tokens={{ childrenGap: 15 }}>
      <TextField label="age" />
      <TextField label="name" />
      <TextField label="street" />
    </Stack>
  </Stack>
);

ReactDOM.render(
  <Fabric>
    <App />
  </Fabric>,
  document.getElementById("root")
);
